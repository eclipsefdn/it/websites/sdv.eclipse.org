---
title: Jury
seo_title: Jury | SDV Hackathon 2023 | Software Defined Vehicle
headline: ' ' 
custom_jumbotron: |
  <h1 class="padding-y-60">The Jury</h1>
hide_page_title: true
container: container
tags: ["Jury", "Hackathon", "Automotive"]
---

{{< events/user_bios event="sdv-hackathon-2023" source="jury" imgRoot="../images/jury/" >}}
