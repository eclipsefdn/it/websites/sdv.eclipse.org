---
title: 'Automotive Processes for Open Source SW'
seo_title: 'Automotive Processes for Open Source SW - Software Defined Vehicle'
keywords: ["special interest group", "software defined vehicle", "sig", "sdv", "automotive", "open source"]
headline: Automotive Processes for Open Source SW
hide_page_title: true
hide_sidebar: true
container: "container"
header_wrapper_class: 'header-alt'
custom_jumbotron: |
      <div>
        <h1>Automotive Processes for Open Source SW</h1>
        <ul class="list-inline">
          <a class="btn btn-neutral" href="#join">Join this SIG</a>
        </ul>    
      </div> 
---

{{< grid/div class="margin-top-20" >}}

## Full-Stack Compliance with Industry Standards { .h1 .margin-bottom-20 .text-center }

In the automotive industry, software security and vehicle safety are tightly interconnected.
This is why adhering to safety standards such as Automotive SPICE (A-SPICE), functional safety (according to ISO2626) and long-term maintenance (compliant with UNECE regulations) is paramount.
Traditionally, the rigorous development processes required for these automotive standards have stifled the adoption of open source software, as it is often believed that open source does not meet the stringent criteria of safety-critical systems.

With [ThreadX](https://threadx.io/), we now have the first Real-Time Operating System (RTOS) certified for safety-critical applications. In addition, the [maturity badges](https://newsroom.eclipse.org/eclipse-newsletter/2024/may/driving-sdv-project-adoption-and-involvement-maturity-badges) introduced by the SDV Working Group will help establish trust in open-source automotive projects by verifying and indicating the technology’s state of enterprise-readiness.

The Automotive Processes for Open Source SW SIG enters the next stage of automotive OSS evolution by developing a complete SDV software stack that meets functional safety requirements. Specifically, the Automotive Grade Open Source SIG aims to:

* Harmonise OSS development with automotive grade maturity models
* Achieve functional safety with OSS
* Investigate the impact of long-term support on OSS
* Address IP and legal issues when integrating open and closed source software

Read the [full SIG proposal](https://www.eclipse.org/lists/sdv-wg/msg00496.html) to learn more.

Review the [meeting notes](https://gitlab.eclipse.org/eclipse-wg/sdv-wg/sdv-sig-automotivegradeos/meetings/-/blob/main/MeetingNotes.md) to check the SIG's progress.

{{</ grid/div >}}

{{< grid/section-container id="leads" class="sig-leads">}}
  {{< events/user_display event="automotive-processes" source="leads" imageRoot="images/" title="Co-Leads" useCarousel="false" itemClass="col-md-push-6 col-xs-12 col-sm-6 col-sm-push-6 match-height-item-by-row">}}
  {{</ events/user_display >}}
{{</ grid/section-container >}}

{{< pages/special-interest-groups/members_list source="automotive-processes" >}}

---

{{< pages/special-interest-groups/how_to_join >}}
