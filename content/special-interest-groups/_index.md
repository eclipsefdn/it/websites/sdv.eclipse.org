---
title: 'Special Interest Groups (SIGs)'
seo_title: 'What Are Special Interest Groups - Software Defined Vehicle'
keywords: ["special interest group", "software defined vehicle", "sig", "sdv", "faq"]
headline: Software Defined Vehicle SIGs
hide_page_title: true
hide_sidebar: true
layout: single
container: "container-fluid"
header_wrapper_class: 'header-alt'
custom_jumbotron: |
      <div>
        <h1>Special Interest Groups (SIGs)</h1>
      </div> 
---

{{< grid/section-container class="margin-top-20" isMarkdown="true" >}}

## What Are Special Interest Groups? { .h1 .margin-bottom-30 .text-center }

The software industry is in a constant state of flux, with new trends, technologies, and requirements emerging almost daily. Keeping pace with these developments can be a daunting challenge – especially within the scope of SDV projects, where the demand for cutting-edge solutions is constant.

This is where our [Special Interest Groups](https://www.eclipse.org/org/workinggroups/operations.php#h.7g2t1bk4a3n9) (SIGs) come into play.

Our SIGs are dedicated to exploring potentially groundbreaking technologies and concepts within the automotive realm. The goal of SIGs is to enable members to share expertise, discovery, and best practices. Each group focuses on a specific innovation or concept, providing a collaborative space for discussion, exploration, and strategic application to enhance automotive businesses.

Think of our SIGs as agile speedboats, swiftly navigating and testing new automotive waters. They are at the forefront of technological advancements, ensuring that the SDV Working Group as a whole stays ahead of the curve. Join a SIG today and be part of a community that drives innovation and leverages the latest breakthroughs in the ever-evolving automotive industry.

{{</ grid/section-container >}}

{{< grid/section-container class="margin-bottom-40 margin-top-30" isMarkdown="true" >}}

## How are these SIGs related to the SDV working group? { .h1 .margin-bottom-20 .text-center }

Any member of the SDV Working Group can propose a new SIG via the [mailing list](https://accounts.eclipse.org/mailing-list/sdv-wg) to drive innovation or promote and support a specific subset of open-source projects and specifications within the Working Group's scope. New proposals must be approved by the Steering Committee. One important criterion for forming a new SIG is that at least three member organisations participate in it.

SIGs do not directly influence the SDV Working Group or open source projects. However, they can identify new trends and topics relevant to the Working Group. These insights can help organisations with committers involved in a project align their priorities with the Working Group's activities.

Discover everything you need to know about SIG formation, goals, and governance in our comprehensive [Working Group Operations Guide](https://www.eclipse.org/org/workinggroups/operations.php#h.7g2t1kbk4a3n9).

{{</ grid/section-container >}}

{{< pages/special-interest-groups/sig_list >}}

{{< pages/special-interest-groups/how_to_join >}}
