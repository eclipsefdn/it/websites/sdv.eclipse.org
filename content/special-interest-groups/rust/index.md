---
title: 'Rust'
seo_title: 'Rust - Software Defined Vehicle'
keywords: ["special interest group", "software defined vehicle", "sig", "sdv", "rust"]
headline: Rust - Software Defined Vehicle
hide_page_title: true
hide_sidebar: true
container: "container"
header_wrapper_class: 'header-alt'
custom_jumbotron: |
      <div>
        <h1>Rust</h1>
        <a class="btn btn-neutral" href="#join">Join this SIG</a>
      </div> 
---

{{< grid/div class="margin-top-20" >}}

## A Next-Generation Programming Language for Embedded Systems { .h1 .margin-bottom-30 .margin-top-20 .text-center }

Rust is a rising star among the programming languages used in embedded and mission-critical systems.
In the [July 2024 Tiobe Index](https://www.tiobe.com/tiobe-index/), it leaped to a new record position, jumping from rank #17 to #13.
In the automotive industry, Rust adoption has increased tremendously over recent years.
Within the Eclipse SDV, several projects are being written in Rust, e.g. Eclipse [SommR](https://projects.eclipse.org/projects/automotive.sommr) or Eclipse [Ankaios](https://projects.eclipse.org/projects/automotive.ankaios).
In addition, Rust provides a rich ecosystem of open source libraries and tools supported by an active community.

Recognising Rust’s potential to revolutionise the development of safety-certifiable open source software for automakers, this SIG aims to be the premier hub for driving the adoption of the Rust programming language in the automotive industry.

Joining the SIG will enable you to collaborate with industry leaders and innovators to spearhead Rust's further development and integration into the automotive sector.

Read the SIG proposal [here](https://www.eclipse.org/lists/sdv-wg/msg00472.html).

Review the [meeting notes](https://drive.google.com/drive/folders/17bJWcRtOszU5tyxiaFU9gIsIMRB6CuaG) to check the SIG's progress.

{{</ grid/div >}}

{{< grid/section-container id="leads" class="sig-leads">}}
  {{< events/user_display event="rust" source="leads" imageRoot="images/" title="Lead" useCarousel="false" itemClass="col-md-offset-9 col-xs-12 col-xs-offset-6 col-sm-6 col-sm-offset-9 match-height-item-by-row">}}
  {{</ events/user_display >}}
{{</ grid/section-container >}}

{{< pages/special-interest-groups/members_list source="rust" >}}

---

{{< pages/special-interest-groups/how_to_join >}}
