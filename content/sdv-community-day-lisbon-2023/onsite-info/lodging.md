---
title: Lodging
weight: 2
---

[Eurostars Universal Lisboa](https://www.eurostarshotels.co.uk/eurostars-universal-lisboa.html)

[Hotel Tivoli Oriente](https://www.tivolihotels.com/en/)

[Ibis Lisboa Parque das Naçoes](https://all.accor.com/hotel/8501/index.pt.shtml)

[Melia Lisboa Oriente](https://www.melia.com/en/hotels/portugal/lisbon/melia-lisboa-oriente?esl-k=sem-google%7Cng%7Cc622667841567%7Cme%7Ckmelia%20lisboa%20oriente%7Cp%7Ct%7Cdc%7Ca132343936310%7Cg14950887732&gclid=CjwKCAiAwc-dBhA7EiwAxPRylF5GDiZXhJtu8jWC3dIqTI0cVJReiPLUWYGQjWUC_BjTxkZ0u30FpRoCNRwQAvD_BwE&gclsrc=aw.ds)

[MYRIAD by SANA](https://www.sanahotels.com/en/hotel/myriad-by-sana/)

[Olissippo Oriente](https://www.guestreservations.com/olissippo-oriente-lisbon/booking?gclid=CjwKCAiAwc-dBhA7EiwAxPRylI1Lj3CijmW_OenBo64hTaWADPzgX_EW-Mij1Fj3i8SoJhTt8OndNBoCRMwQAvD_BwE)