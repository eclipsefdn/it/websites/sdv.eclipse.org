---
title: What is SDV
seo_title: What is SDV | Software Defined Vehicle
headline: Join the Software Defined Vehicle Working Group
hide_page_title: true
hide_sidebar: true
container: "container"
header_wrapper_class: 'header-alt'
custom_jumbotron: |
      <div>
        <h1>About SDV</h1>
      </div>       
---

{{< grid/div class="margin-top-20" >}}

## What is the Software Defined Vehicle? { .h1 .margin-bottom-30 .text-center }

The term software-defined vehicle refers to a transformation where the physical and digital components of an automobile are decoupled and features, functionality, and operations are defined through software. In a fully programmable car, digital components—such as modules for safety, comfort and infotainment, and vehicle performance—would be regularly developed and deployed through over-the-air updates.

The Eclipse Software Defined Vehicle (SDV) Working Group facilitates open source development of this software. Learn more about the [SDV working group](/about-the-working-group).

{{</ grid/div >}}

{{< grid/div class="margin-bottom-40 margin-top-30" >}}

## Why Open Source? { .h1 .margin-bottom-30 .text-center }

Current automotive industry paradigms pose challenges to achieving the SDV vision. Today’s car models contain custom hardware and software components sourced from many suppliers. Hardware and software components are tightly integrated and released simultaneously. This results in fragmentation and monolithic programming frameworks. The challenge is illustrated in the following consideration: of the some 100 million lines of code that make up the modern car, it’s estimated that mainstream OS installations of Linux, Windows and OS X share a lot more code between each other than vehicles from any two OEMs.

Open source is a way to confront this complexity and heterogeneity. Collaborating on the technological framework—such as common APIs and hardware abstraction—means OEMs and other industry players do not need to reinvent the wheel, allowing more resources to be devoted to differentiating features and technological advancements. An open source ecosystem also has the potential to accelerate innovation by enabling developers worldwide to solve problems and create applications in the automotive domain.

{{</ grid/div >}}
