---
title: "Lodging"
date: 2022-08-10T12:51:46-04:00
description: ""
categories: []
keywords: []
slug: ""
aliases: []
toc: false
draft: false
#header_wrapper_class: ""
#seo_title: ""
#headline: ""
#subtitle: ""
#tagline: ""
#links: []
---
[Maritim Hotel Bonn](https://www.maritim.com/en/hotels/germany/hotel-bonn/hotel-overview)

[Kameha Grand Hotel](https://www.kamehabonn.de/en/)

[Bonn Marriott Hotel](https://www.marriott.com/en-us/hotels/cgnbo-bonn-marriott-hotel/overview/?scid=f2ae0541-1279-4f24-b197-a979c79310b0)

[Living Hotel Kanzler](https://www.living-hotels.com/hotel-kanzler-bonn/en/)

[Area Information](https://www.google.com/maps/search/Hotels/@50.7095428,7.117626,14z/data=!4m8!2m7!3m6!1sHotels!2sDeutsche+Telekom+Aktiengesellschaft,+Friedrich-Ebert-Allee+140,+53113+Bonn!3s0x47bee3e16ea2c9f5:0x3c39dcf8c6429d11!4m2!1d7.1287377!2d50.7077659)